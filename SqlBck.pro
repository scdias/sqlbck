#-------------------------------------------------
#
# Project created by QtCreator 2017-08-12T21:44:30
#
#-------------------------------------------------

QT       += core gui network sql concurrent
CONFIG   += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SqlBck
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QUAZIP_STATIC
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    utils.cpp \
    config.cpp \
    mssql.cpp \
    basedados.cpp \
    sobre.cpp \
    log.cpp

HEADERS += \
        mainwindow.h \
    utils.h \
    config.h \
    mssql.h \
    basedados.h \
    sobre.h \
    log.h

FORMS += \
        mainwindow.ui \
    sobre.ui

RESOURCES += \
    resources.qrc

#Adcionar info ao executável
win32:RC_ICONS += resources/if_Graphite_Backup_B_66569.ico

VERSION = 1.0.0.5
QMAKE_TARGET_COMPANY = scdias@outlook.com
QMAKE_TARGET_DESCRIPTION = SQL Backup
QMAKE_TARGET_COPYRIGHT = (C) 2018 scdias@outlook.com
QMAKE_TARGET_PRODUCT = SQL Backup
DEFINES += APP_VERSION=\"\\\"$${VERSION}\\\"\" \

#Criar constantes para gerir versões de bibliotecas

VERSION_QUAZIP = 0.7.6
DEFINES += VERSION_QUAZIP_STR=\"\\\"$${VERSION_QUAZIP}\\\"\" \

#Adciona quazip ao projeto
win32: LIBS += -L$$PWD/libs/quazip/ -lquazip

INCLUDEPATH += $$PWD/libs/quazip
DEPENDPATH += $$PWD/libs/quazip

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs/quazip/quazip.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs/quazip/libquazip.a

#Adciona zlib ao projeto
win32: LIBS += -L$$PWD/libs/zlib/ -lzlibstatic

INCLUDEPATH += $$PWD/libs/zlib
DEPENDPATH += $$PWD/libs/zlib

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs/zlib/zlibstatic.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs/zlib/libzlibstatic.a
