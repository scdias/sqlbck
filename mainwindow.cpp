#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utils.h"
#include "config.h"
#include <QFileDialog>
#include "mssql.h"
#include "sobre.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(size());
    ui->cbox_servidor->addItems(Utils::ListaServidoresSQL());

    //Ler Servidor SQL
    QString configServidor = config::getServidor();
    if (!configServidor.isEmpty()) ui->cbox_servidor->setCurrentText(configServidor);

    //Ler Utilizador SQL
    QString configUtilizador = config::getUtilizador();
    if (!configUtilizador.isEmpty()) ui->txt_utilizador->setText(configUtilizador);

    //Ler Password SQL
    QString configPassword = config::getPassword();
    if (!configPassword.isEmpty()) ui->txt_password->setText(configPassword);

    //Ler Diretório de cópias
    QString configDiretorio = config::getDiretorio();
    if (!configDiretorio.isEmpty()) ui->txt_diretorio->setText(configDiretorio);

    //Ler Encriptação
    ui->chkbox_encriptacao->setChecked(config::getEncriptacao());

    //Ler Compressão
    ui->chkbox_compressao->setChecked(config::getCompactacao());

    //Ler Compressão Zip
    ui->chkbox_compressao_zip->setChecked(config::getCompactacaoZip());

    //Ler Timestamp
    ui->chkbox_timestamp->setChecked(config::getTimestamp());

    if (config::getValido())
    {
        ui->btn_executar->setDisabled(false);
        ui->btn_salvar->setDisabled(false);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    mssql::fechaLigacao();
    config::FechaConfig();
}

bool MainWindow::ValidaDados()
{
    bool Valido = true;

    //Validar Servidor
    if (ui->cbox_servidor->currentText().isEmpty())
    {
        ui->cbox_servidor->setStyleSheet("QComboBox {background-color: red;}");
        Valido = false;
    } else {
        ui->cbox_servidor->setStyleSheet("");
    }

    //Validar Utilizador
    if(ui->txt_utilizador->text().isEmpty())
    {
        ui->txt_utilizador->setStyleSheet("QLineEdit {background-color: red;}");
        Valido = false;
    } else {
        ui->txt_utilizador->setStyleSheet("");
    }

    //Validar Password
    if(ui->txt_password->text().isEmpty())
    {
        ui->txt_password->setStyleSheet("QLineEdit {background-color: red;}");
        Valido = false;
    } else {
        ui->txt_password->setStyleSheet("");
    }

    //Validar Diretorio
    if(ui->txt_diretorio->text().isEmpty())
    {
        ui->txt_diretorio->setStyleSheet("QLineEdit {background-color: red;}");
        Valido = false;
    } else {
        ui->txt_diretorio->setStyleSheet("");
    }
    return Valido;
}

void MainWindow::GuardaConfig()
{
    config::setServidor(ui->cbox_servidor->currentText());
    config::setUtilizador(ui->txt_utilizador->text());
    config::setPassword(ui->txt_password->text());
    config::setDiretorio(ui->txt_diretorio->text());
    config::setEncriptacao(ui->chkbox_encriptacao->isChecked());
    config::setCompactacao(ui->chkbox_compressao->isChecked());
    config::setCompactacaoZip(ui->chkbox_compressao_zip->isChecked());
    config::setTimestamp(ui->chkbox_timestamp->isChecked());
    config::GuardaConfig();
}

void MainWindow::on_btn_executar_clicked()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    if (ValidaDados())
    {
        GuardaConfig();
        Utils::reiniciarAplicacao("unico");
    }
    QApplication::restoreOverrideCursor();
}

void MainWindow::on_btn_salvar_clicked()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    if (ValidaDados()) GuardaConfig();
    QApplication::restoreOverrideCursor();
}

void MainWindow::on_btn_folder_clicked()
{
    ui->txt_diretorio->setText(QFileDialog::getExistingDirectory(this,"Diretório para cópias",config::getDiretorio(),
                                                                 QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
}

void MainWindow::on_btn_conectar_clicked()
{
    config::setValido(mssql::testaConeccao(ui->cbox_servidor->currentText(),
                                           ui->txt_utilizador->text(),
                                           ui->txt_password->text()));

    if(config::getValido())
    {
        ui->btn_executar->setDisabled(false);
        ui->btn_salvar->setDisabled(false);
    } else {
        ui->btn_executar->setDisabled(true);
        ui->btn_salvar->setDisabled(true);
    }

    bool edicaoPro = mssql::edicaoProfissional();

    ui->chkbox_compressao->setChecked(edicaoPro);
    ui->chkbox_compressao->setEnabled(edicaoPro);

}

void MainWindow::on_cbox_servidor_editTextChanged()
{
    ui->btn_executar->setDisabled(true);
    ui->btn_salvar->setDisabled(true);
}

void MainWindow::on_txt_utilizador_textEdited()
{
    ui->btn_executar->setDisabled(true);
    ui->btn_salvar->setDisabled(true);
}

void MainWindow::on_txt_password_textEdited()
{
    ui->btn_executar->setDisabled(true);
    ui->btn_salvar->setDisabled(true);
}

void MainWindow::on_btn_sobre_clicked()
{
    Sobre FRM_Sobre;
    FRM_Sobre.exec();
}
