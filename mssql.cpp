#include "mssql.h"
#include <QtSql>
#include "utils.h"
#include <QList>
#include "basedados.h"
#include "config.h"
#include <QTime>

void mssql::criarLigacaoSQL(QString Servidor, QString Utilizador, QString Password)
{
    if (QSqlDatabase::contains("SQLServer"))
    {
        QSqlDatabase::removeDatabase("SQLServer");
    }
    QSqlDatabase svr_sql = QSqlDatabase::addDatabase("QODBC","SQLServer");
    QString connString ="DRIVER=SQL Server;"
                        "SERVER=" + Servidor + ";"
                        "DATABASE=master;"
                        "APP=SqlBck by scdias@outlook.com";

    svr_sql.setDatabaseName(connString);
    svr_sql.setUserName(Utilizador);
    svr_sql.setPassword(Password);

    if (!svr_sql.open())
    {
        Utils::log(svr_sql.lastError().text());
        return;
    }
}

void mssql::fazCopia()
{
    QSqlDatabase SqlSvr = QSqlDatabase::database("SQLServer");
    if (!SqlSvr.isOpen()) SqlSvr.open();

    if (!config::getValido()) return;

    QString strQuery = "SELECT sys.databases.name  as BaseDados, "
                       "sys.master_files.size*8/1024 as Tamanho_MB "
                       "FROM sys.databases "
                       "INNER JOIN sys.master_files ON sys.databases.database_id=sys.master_files.database_id "
                       "WHERE sys.databases.name NOT IN ('master','tempdb','model','msdb') "
                       "AND sys.master_files.data_space_id = 1;";

    QSqlQuery query(SqlSvr);
    query.setForwardOnly(true);
    QList<BaseDados> lst_basedados;

    if (!query.exec(strQuery)){
        Utils::log("Erro a esecutar backup: "+query.lastError().text());
    }

    while (query.next())
    {
        lst_basedados.append(BaseDados(query.value("BaseDados").toString(),query.value("Tamanho_MB").toInt()));
    }

    query.finish();

    int total_mb = 0;
    foreach (BaseDados bd, lst_basedados) {
        total_mb = total_mb + bd.getTamanho_mb();
    }

    Utils::log("A iniciar copias....");
    int parcial_mb = 0;
    int perc = 0;
    int c = 0;
    QTime inicio = QTime::currentTime();
    foreach (BaseDados bd, lst_basedados) {
        c++;
        perc = (int)parcial_mb*100/total_mb;
        Utils::log(QString::number(perc) + "% completo. A fazer "
                   + QString::number(c) + "/" + QString::number(lst_basedados.length())+":"+ bd.getNome());
        fazCopiaBD(bd.getNome(),config::getDiretorio());
        parcial_mb = parcial_mb + bd.getTamanho_mb();
        QString Origem = config::getDiretorio() + "\\" + bd.getNome() + Utils::daDataHora(config::getTimestamp()) +".DAT";
        QString Destino = config::getDiretorio() + "\\" + bd.getNome() + Utils::daDataHora(config::getTimestamp()) +".ZIP";
        Utils::IniciaCompactacao(Origem, Destino);
    }
    QTime fim = QTime::currentTime();
    int seg = inicio.secsTo(fim);
    Utils::log("Copia finalizada em "  +QString::number(seg/60) + " minutos "
                                                                  "(" + QString::number(total_mb/seg)  +"MB/s).");
}

void mssql::fechaLigacao()
{
    QSqlDatabase SqlSvr = QSqlDatabase::database("SQLServer");
    SqlSvr.close();
    Utils::log("Ligacao SQLServer terminada.");
}

bool mssql::edicaoProfissional()
{
    QSqlDatabase SqlSvr = QSqlDatabase::database("SQLServer");
    if (!SqlSvr.isOpen()) SqlSvr.open();
    QSqlQuery query("select serverproperty('EngineEdition')",SqlSvr);

    if (query.first())
    {
        QString Valor = query.value(0).toString();
        if (Valor == "\004") return false; // 4 = Express Edition
        else return true;
    }

    Utils::log("Erro a determinar a edicao do Servidor.");
    Utils::log(">> " + query.lastError().text());
    return false;

}

void mssql::fazCopiaBD(QString baseDados, QString destino)
{
    QSqlDatabase SqlSvr = QSqlDatabase::database("SQLServer");
    if (!SqlSvr.isOpen()) SqlSvr.open();

    QString strQuery;

    QString compactacao_qry = "";
    if (config::getCompactacao())
    {
        compactacao_qry = "COMPRESSION, ";
    }

    strQuery = "BACKUP DATABASE [" + baseDados +"]"
               "TO  DISK = N'" + destino + "\\"+ baseDados + Utils::daDataHora(config::getTimestamp()) +".DAT' WITH "
               "NAME = N'" + baseDados + "', "
               "DESCRIPTION = N'"+"Full Backup by SqlBck"+"', "
               "COPY_ONLY,"
               "NOFORMAT, "
               "INIT, "
               "SKIP, "
               "NOREWIND, "
               "NOUNLOAD, "
               "STATS = 10, "
               + compactacao_qry +
               "CHECKSUM";

    QSqlQuery query(SqlSvr);
    query.setForwardOnly(true);

    if (!query.exec(strQuery))
    {
        Utils::log("Erro Backup: " + query.lastError().text());
    }

    query.finish();

}

bool mssql::testaConeccao(QString Servidor, QString Utilizador, QString Password)
{
    if (QSqlDatabase::contains("TesteAuth"))
    {
        QSqlDatabase::removeDatabase("TesteAuth");
    }

    QSqlDatabase svr_sql = QSqlDatabase::addDatabase("QODBC","TesteAuth");
    QString connString ="DRIVER=SQL Server;"
                        "SERVER=" + Servidor + ";"
                        "DATABASE=master;"
                        "APP=SqlBck by scdias@outlook.com";

    svr_sql.setDatabaseName(connString);
    svr_sql.setUserName(Utilizador);
    svr_sql.setPassword(Password);

    if (!svr_sql.open())
    {
        Utils::log(svr_sql.lastError().text());
        return false;
    }

    QSqlQuery query(svr_sql);
    if (!query.exec("SELECT @@Version"))
    {
        Utils::log(query.lastError().text());
        svr_sql.close();
        return false;
    }

    if(!query.first()){
        Utils::log(query.lastError().text());
        svr_sql.close();
        return false;
    }

    Utils::log(query.value(0).toString());
    criarLigacaoSQL(Servidor,Utilizador,Password);
    svr_sql.close();
    return true;
}
