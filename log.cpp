#include "log.h"
#include <QtSql>
#include "config.h"

int Log::getDataHora() const
{
    return dataHora;
}

void Log::setDataHora(int value)
{
    dataHora = value;
}

QString Log::getDataHoraTXT() const
{
    return dataHoraTXT;
}

void Log::setDataHoraTXT(const QString &value)
{
    dataHoraTXT = value;
}

QString Log::getMensagem() const
{
    return mensagem;
}

void Log::setMensagem(const QString &value)
{
    mensagem = value;
}

void Log::gravar()
{
    if (!QSqlDatabase::database("config").isValid()) return;
    QSqlDatabase configDB = QSqlDatabase::database("config");
    if (!configDB.isOpen()) configDB.open();
    if (!configDB.isOpen()) return;

    if(config::getVersao().isEmpty()) return;

    QSqlQuery query(configDB);
    query.prepare("INSERT INTO `Log`"
                  "(`DataHora`,`DataHoraTXT`,`Mensagem`) "
                  "VALUES "
                  "(:Datahora,:DataHoraTXT,:Mensagem);");

    query.bindValue(":Datahora",this->dataHora);
    query.bindValue(":DataHoraTXT",this->dataHoraTXT);
    query.bindValue(":Mensagem",this->mensagem);

    if(!query.exec())
    {
        return;
    }
    query.finish();
}

Log::Log()
{

}

Log::Log(int dataHora, QString dataHoraTXT, QString mensagem)
{
    this->dataHora = dataHora;
    this->dataHoraTXT = dataHoraTXT;
    this->mensagem = mensagem;
}
