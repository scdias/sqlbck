CREATE TABLE `Log` (
        `DataHora`	INTEGER NOT NULL UNIQUE,
        `DataHoraTXT`	TEXT,
        `Mensagem`	TEXT,
        PRIMARY KEY(`DataHora`)
);
CREATE TABLE "Config" (
	`Versao`	TEXT NOT NULL,
	`Servidor`	TEXT,
	`Utilizador`	TEXT,
	`Password`	TEXT,
	`Valido`	TEXT,
	`Diretorio`	TEXT,
	`Encriptacao`	TEXT,
	`Compactacao`	TEXT,
	`EdicaoProfissional`	TEXT,
	`OpenPGP`	TEXT,
	PRIMARY KEY(`Versao`)
);
INSERT INTO `Config` VALUES ('1.0.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
