#ifndef LOG_H
#define LOG_H
#include <QString>

/**
 * @brief Classe resposável pelo armazenamento de logs de execução
 */
class Log
{
private:
    /**
     * @brief DataHora no formato unix
     */
    int dataHora;

    /**
     * @brief Data hora por extenso
     */
    QString dataHoraTXT;

    /**
     * @brief Mensagem a ser registada pela aplicação
     */
    QString mensagem;

public:
    /**
     * @brief Construtor genérico
     */
    Log();

    /**
     * @brief Construtor com argumentos
     * @param dataHora - DataHora no formato unix
     * @param dataHoraTXT - Data hora por extenso
     * @param mensagem - Mensagem a ser registada pela aplicação
     */
    Log(int dataHora, QString dataHoraTXT, QString mensagem);

    /**
     * @brief DataHora no formato unix
     * @return int com Datahora no formato unix
     */
    int getDataHora() const;

    /**
     * @brief DataHora no formato unix
     * @param value - int com Datahora no formato unix
     */
    void setDataHora(int value);

    /**
     * @brief Data hora por extenso
     * @return define a data e hora por extenso
     */
    QString getDataHoraTXT() const;

    /**
     * @brief Data hora por extenso
     * @param value - QString com a data e hora por extenso a ser definida
     */
    void setDataHoraTXT(const QString &value);

    /**
     * @brief Mensagem a ser registada pela aplicação
     * @return QString com a mensagem registada na aplicação
     */
    QString getMensagem() const;

    /**
     * @brief Mensagem a ser registada pela aplicação
     * @param value - QString com a mensagem a ser registada pela aplicação
     */
    void setMensagem(const QString &value);

    /**
     * @brief Grava o informação de log na base de dados do config
     */
    void gravar();

};

#endif // LOG_H
