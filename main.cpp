#include "mainwindow.h"
#include <QApplication>
#include "windows.h"
#include "config.h"
#include "utils.h"
#include <QtCore/QCoreApplication>
#include "mssql.h"
#include <QtConcurrent>

int main(int argc, char *argv[])
{
    SetConsoleTitle(TEXT("SqlBck by scdias@outlook.com"));
    QCoreApplication::setApplicationVersion(QString(APP_VERSION));
    QApplication a(argc, argv);
    config::IniciarConfig();
    config::LerConfig();

    if (QCoreApplication::arguments().length() > 1)
    {
        foreach (QString parametro, QCoreApplication::arguments()) {
            if (parametro == "unico")
            {
                Utils::log("Execucao de tarefa unica");
                mssql::criarLigacaoSQL(config::getServidor(),config::getUtilizador(),config::getPassword());
                mssql::fazCopia();
                mssql::fechaLigacao();
                config::FechaConfig();
                Utils::reiniciarAplicacao("");
                return (0);
            } else {
                if (parametro == "tarefa")
                {
                    Utils::log("Execucao de tarefa agendada");
                    mssql::criarLigacaoSQL(config::getServidor(),config::getUtilizador(),config::getPassword());
                    mssql::fazCopia();
                    config::FechaConfig();
                    mssql::fechaLigacao();
                    return (0);
                }
            }
        }
    }

    MainWindow w;
    w.show();
    return a.exec();
}
