#include "config.h"
#include <QString>
#include "utils.h"
#include <QtSql>

QString config::Versao;
QString config::Servidor;
QString config::Utilizador;
QString config::Password;
QString config::Diretorio;
bool config::Valido;
bool config::Encriptacao;
bool config::Compactacao;
bool config::CompactacaoZip;
bool config::EdicaoProfissional;
bool config::OpenPGP;
bool config::Timestamp;

QString config::getVersao()
{
    return config::Versao;
}

void config::setVersao(const QString &value)
{
    Versao = value;
}

QString config::getServidor()
{
    return Servidor;
}

void config::setServidor(const QString &value)
{
    Servidor = value;
}

QString config::getUtilizador()
{
    return Utilizador;
}

void config::setUtilizador(const QString &value)
{
    Utilizador = value;
}

QString config::getPassword()
{
    return Password;
}

void config::setPassword(const QString &value)
{
    Password = value;
}

QString config::getDiretorio()
{
    return Diretorio;
}

void config::setDiretorio(const QString &value)
{
    Diretorio = value;
}

bool config::getValido()
{
    return Valido;
}

void config::setValido(bool value)
{
    Valido = value;
}

bool config::getEncriptacao()
{
    return Encriptacao;
}

void config::setEncriptacao(bool value)
{
    Encriptacao = value;
}

bool config::getCompactacao()
{
    return Compactacao;
}

void config::setCompactacao(bool value)
{
    Compactacao = value;
}

bool config::getEdicaoProfissional()
{
    return EdicaoProfissional;
}

void config::setEdicaoProfissional(bool value)
{
    EdicaoProfissional = value;
}

bool config::getOpenPGP()
{
    return OpenPGP;
}

void config::setOpenPGP(bool value)
{
    OpenPGP = value;
}

bool config::getCompactacaoZip()
{
    return CompactacaoZip;
}

void config::setCompactacaoZip(bool value)
{
    CompactacaoZip = value;
}

void config::IniciarConfig()
{
    Utils::log("A abrir config....");
    QSqlDatabase configDB = QSqlDatabase::addDatabase("QSQLITE","config");
    configDB.setDatabaseName(qApp->applicationDirPath() + "\\SqlBck.db");
    configDB.open();

    QSqlQuery query("SELECT Versao FROM Config Where _rowid_ = 1",configDB);
    if (!query.first()) CriarEstrutura();
    else
    {
        QString db_ver = query.value("Versao").toString();
        if (db_ver == "1.0.0") UpgradeDbV1_0_1();
        if (db_ver == "1.0.1") UpgradeDbV1_0_2();
    }
}

void config::FechaConfig()
{
    QSqlDatabase configDB = QSqlDatabase::database("config");
    configDB.close();
}

void config::LerConfig()
{
    QSqlDatabase configDB = QSqlDatabase::database("config");
    QSqlQuery query("SELECT * FROM Config",configDB);

    if(query.first())
    {
        Versao = query.value("Versao").toString();
        Servidor = query.value("Servidor").toString();
        Utilizador = query.value("Utilizador").toString();
        Password = query.value("Password").toString();
        Diretorio = query.value("Diretorio").toString();
        Valido = query.value("Valido").toBool();
        Encriptacao = query.value("Encriptacao").toBool();
        Compactacao = query.value("Compactacao").toBool();
        CompactacaoZip = query.value("CompactacaoZip").toBool();
        EdicaoProfissional = query.value("EdicaoProfissional").toBool();
        OpenPGP = query.value("OpenPGP").toBool();
        Timestamp = query.value("TimeStamp").toBool();
    }else {
        Utils::log("Erro a ler config: "+query.lastError().text());
    }
    query.finish();
}

void config::GuardaConfig()
{
    QSqlDatabase configDB = QSqlDatabase::database("config");
    QSqlQuery query(configDB);
    query.prepare("UPDATE Config "
                  "SET Servidor = :Servidor, "
                  "Utilizador = :Utilizador, "
                  "Password = :Password, "
                  "Valido = :Valido, "
                  "Diretorio = :Diretorio, "
                  "Encriptacao = :Encriptacao, "
                  "Compactacao = :Compactacao, "
                  "CompactacaoZip = :CompactacaoZip, "
                  "EdicaoProfissional = :EdicaoProfissional, "
                  "OpenPGP = :OpenPGP, "
                  "TimeStamp = :TimeStamp "
                  "WHERE Versao = :Versao");

    query.bindValue(":Servidor",Servidor);
    query.bindValue(":Utilizador",Utilizador);
    query.bindValue(":Password",Password);
    query.bindValue(":Diretorio",Diretorio);
    query.bindValue(":Valido",Valido);
    query.bindValue(":Encriptacao",Encriptacao);
    query.bindValue(":Compactacao",Compactacao);
    query.bindValue(":CompactacaoZip",CompactacaoZip);
    query.bindValue(":EdicaoProfissional",EdicaoProfissional);
    query.bindValue(":OpenPGP",OpenPGP);
    query.bindValue(":TimeStamp",Timestamp);
    query.bindValue(":Versao",Versao);

    configDB.transaction();

    if (query.exec())
    {
        configDB.commit();
    } else {
        Utils::log(query.lastError().text());
        configDB.rollback();
    }
}

void config::CriarEstrutura()
{
    QSqlDatabase configDB = QSqlDatabase::database("config");
    QSqlQuery query(configDB);
    QFile resource (":/Queries/Config V1.0.0");
    resource.open(QFile::ReadOnly | QFile::Text);
    QTextStream conteudo(&resource);
    QStringList createDB = conteudo.readAll().split(";");

    configDB.transaction();
    Utils::log("A Criar ficheiro de configuracao....");

    foreach (QString querytxt, createDB) {
        if (querytxt.trimmed().isEmpty()){
            continue;
        }
        if (!query.exec(querytxt)){
            Utils::log(query.lastError().text());
            Utils::log(querytxt);
            return;
        }
    }
    query.finish();
    configDB.commit();

    UpgradeDbV1_0_1();
}

void config::UpgradeDbV1_0_1()
{
    QSqlDatabase configDB = QSqlDatabase::database("config");
    QSqlQuery query(configDB);
    QFile resource (":/Queries/Config V1.0.1");
    resource.open(QFile::ReadOnly | QFile::Text);
    QTextStream conteudo(&resource);
    QStringList createDB = conteudo.readAll().split(";");

    configDB.transaction();
    Utils::log("A Fazer upgrade para V1.0.1....");

    foreach (QString querytxt, createDB) {
        if (querytxt.trimmed().isEmpty()){
            continue;
        }
        if (!query.exec(querytxt)){
            Utils::log(query.lastError().text());
            Utils::log(querytxt);
            return;
        }
    }
    query.finish();
    configDB.commit();
    UpgradeDbV1_0_2();
}

void config::UpgradeDbV1_0_2()
{
    QSqlDatabase configDB = QSqlDatabase::database("config");
    QSqlQuery query(configDB);
    QFile resource (":/Queries/Config V1.0.2");
    resource.open(QFile::ReadOnly | QFile::Text);
    QTextStream conteudo(&resource);
    QStringList createDB = conteudo.readAll().split(";");

    configDB.transaction();
    Utils::log("A Fazer upgrade para V1.0.2....");

    foreach (QString querytxt, createDB) {
        if (querytxt.trimmed().isEmpty()){
            continue;
        }
        if (!query.exec(querytxt)){
            Utils::log(query.lastError().text());
            Utils::log(querytxt);
            return;
        }
    }
    query.finish();
    configDB.commit();
}

bool config::getTimestamp()
{
    return Timestamp;
}

void config::setTimestamp(bool value)
{
    Timestamp = value;
}
