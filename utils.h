#ifndef UTILS_H
#define UTILS_H
#include <QStringList>
#include <QString>

/**
 * @brief Classe com funções genéricas usadas em toda aplicação
 */
class Utils
{
public:
    /**
     * @brief Define TimeStamp a adcionar no ficheiro
     * @param True: define TimeStamp, False: Não coloca Timestamp no ficheiro
     * @return QString com data/hora actual no formato YYYYMMDD_HHMMSS
     */
    static QString daDataHora(bool Timestamp);

    /**
     * @brief Localiza as instancias de SQL instaladas no próprio computador
     * @return QStringList com endereço de ligação das instâncias
     */
    static QStringList ListaServidoresSQL();

    /**
     * @brief Função responsavel pelo registo de logs da aplicação
     * @param Mensagem - QString com a mensagem a ser guardada no log
     */
    static void log(QString Mensagem);

    /**
     * @brief Módulo responsável pelo reinício da aplicação
     * @param modo - QString com modo de reício da aplicação (Cópia unica / tarefa)
     */
    static void reiniciarAplicacao(QString modo);

    /**
     * @brief Inicia Thread para compactação de ficheiros
     * @param Origem - Caminho do ficheiro a ser compactado
     * @param Destino - Caminho do ficheiro Zip
     */
    static void IniciaCompactacao(QString Origem, QString Destino);

private:
    /**
     * @brief Devolve a versão do SQL instalado
     * @param Chave de registo indicativo da instância
     * @return Versão do SQL
     */
    static QString VersaoSQL(QString ValorChave);

    /**
     * @brief Compacta o ficheiro em formato Zip
     * @param CaminhoFicheiro - QString com o ficheiro que será compactado
     * @param CaminhoZip - QString com o destino do ficheiro Zip
     */
    static void CompactarZip(QString CaminhoFicheiro, QString CaminhoZip);

};

#endif // UTILS_H
