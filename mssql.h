#ifndef MSSQL_H
#define MSSQL_H
#include <QString>

/**
 * @brief Classe com todas funções relativas ao Servidor MS SQL
 */
class mssql
{
public:
    /**
     * @brief Cria a ligação a base de dados SQL
     */
    static void criarLigacaoSQL(QString Servidor, QString Utilizador, QString Password);

    /**
     * @brief Faz cópias de todas as bases de dados
     */
    static void fazCopia();

    /**
     * @brief Fecha a ligação com o servidor sql
     */
    static void fechaLigacao();

    /**
     * @brief Identifica se a Edicao do SQL é Profissional ou Gratuita
     * @return bool com true se Profissional ou false se express
     */
    static bool edicaoProfissional();

    /**
     * @brief Testa se os dados para a conecção ao servidor SQL são válidos
     * @param Servidor - QString com o endereço do servidor
     * @param Utilizador - QString com o utilizador da autenticação
     * @param Password - QString com a password utilizada na autenticação
     * @return bool se os valores fornecidos são corretos
     */
    static bool testaConeccao(QString Servidor, QString Utilizador, QString Password);

private:
    /**
     * @brief Executa a tarefa de backup da base de dados
     * @param baseDados - QString com o nome da base de dados
     * @param destino - QString com o caminho para onde será feita a cópia
     */
    static void fazCopiaBD(QString baseDados, QString destino);

};

#endif // MSSQL_H
