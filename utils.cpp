#include "utils.h"
#include <QSettings>
//#include <QDebug>
#include <QHostInfo>
#include <QtSql/QSqlDatabase>
#include <QDateTime>
#include <QProcess>
#include <QApplication>
#include "mssql.h"
#include "config.h"
#include "log.h"
#include <QDateTime>
#include "JlCompress.h"
#include "QFile"
#include <QtConcurrent>

QThreadPool pool;

QString Utils::daDataHora(bool Timestamp)
{
    if (!Timestamp) return "";
    QDateTime dataHora = QDateTime::currentDateTime();
    return dataHora.toString("_yyyyMMddHHmm");
}

QStringList Utils::ListaServidoresSQL()
{
    log("A iniciar procura de Instancias SQL");
    QString chave = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Microsoft SQL Server\\Instance Names\\SQL";
    QSettings reg (chave,QSettings::Registry64Format);
    QStringList lstreg = reg.childKeys();
    int i = 0;
    if (lstreg.length()>0)
    {
        log("Encontrado as seguintes instancias:");
        foreach (QString valor, lstreg) {
            log( "     " + QHostInfo::localHostName()+ "\\" + valor +
                 " - "+ "Versao: " + VersaoSQL(reg.value(valor).toString()));
            lstreg[i++] = QHostInfo::localHostName() + "\\" + valor;
        }
    }
    return lstreg;
}

void Utils::log(QString Mensagem)
{
    QDebug debug = qDebug();
    debug.noquote();
    debug << Mensagem;
    QDateTime timestamp = QDateTime::currentDateTime();

    Log registo(timestamp.toTime_t(),
                timestamp.toString("yyyy/MM/dd_HH:mm:ss"),
                Mensagem);
    registo.gravar();

}

void Utils::reiniciarAplicacao(QString modo)
{
    QProcess* processo = new QProcess();
    QStringList parametros;
    parametros.append(modo);
    processo->startDetached(qApp->applicationDirPath()+"/"+qAppName()+".exe",parametros);
    qApp->exit();
}

void Utils::IniciaCompactacao(QString Origem, QString Destino)
{
    if (config::getCompactacaoZip())
    {
        QtConcurrent::run(&pool, CompactarZip, Origem, Destino);
    }

}

QString Utils::VersaoSQL(QString ValorChave)
{
    QString chave1 = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Microsoft SQL Server\\";
    QString chave2 = "\\Setup";
    QSettings reg (chave1 + ValorChave + chave2,QSettings::Registry64Format);
    return reg.value("Version").toString();
}

void Utils::CompactarZip(QString CaminhoFicheiro, QString CaminhoZip)
{
    log("A compactar " + QFile(CaminhoFicheiro).fileName());
    bool ZipOk = JlCompress::compressFile(CaminhoZip,CaminhoFicheiro);
    if (!ZipOk)
    {
        log("Erro em compactar ficheiro " + CaminhoFicheiro);
        return;
    }

    QFile Ficheiro(CaminhoFicheiro);
    Ficheiro.remove();


}
