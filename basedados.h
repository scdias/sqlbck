#ifndef BASEDADOS_H
#define BASEDADOS_H
#include <QString>

/**
 * @brief Classe responsável pelo armazenamento de informações das bases de dados
 */
class BaseDados
{
private:
    /**
     * @brief Nome da base de dados
     */
    QString nome;

    /**
     * @brief Tamanho do ficheiro da base de dados para ser usado em estatística
     */
    int tamanho_mb;
public:
    /**
     * @brief Construtor genérico
     */
    BaseDados();

    /**
     * @brief Construtor com parametros
     * @param nome - Nome da base de dados
     * @param tamanho_mb - Tamanho do ficheiro da base de dados para ser usado em estatística
     */
    BaseDados(QString nome, int tamanho_mb);

    /**
     * @brief Nome da base de dados
     * @return QString -  Nome da base de dados
     */
    QString getNome();

    /**
     * @brief Nome da base de dados
     * @param value - QString - Nome da base de dados
     */
    void setNome(QString &value);
    /**
     * @brief Tamanho do ficheiro da base de dados para ser usado em estatística
     * @return - long long int - Tamanho do ficheiro da base de dados
     */
    int getTamanho_mb();

    /**
     * @brief Tamanho do ficheiro da base de dados para ser usado em estatística
     * @param value - long long int - Tamanho do ficheiro da base de dados
     */
    void setTamanho_mb(int value);
};

#endif // BASEDADOS_H
