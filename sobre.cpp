#include "sobre.h"
#include "ui_sobre.h"
#include "zlib.h"

Sobre::Sobre(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Sobre)
{
    ui->setupUi(this);
    ui->label_3->setText("<p style=\"text-align: right;\"><em>Vers&atilde;o : "
                         +QCoreApplication::applicationVersion()
                         +" </em></p>");
    ui->label_5->setText("<p style=\"text-align: left;\">Vers&atilde;o Qt: " + QString(QT_VERSION_STR)
                         + "<br>Vers&atilde;o ZLib: " + zlibVersion()
                         + "<br>Vers&atilde;o Quazip: " + VERSION_QUAZIP_STR
                         +" </p>");
}

Sobre::~Sobre()
{
    delete ui;
}

