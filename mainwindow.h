#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

/**
 * @brief Janela Principal da aplicação
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * @brief Verifica se os dados inseridos no MainWindow são válidos
     * @return bool com o valor se os dados são válidos
     */
    bool ValidaDados();

    /**
     * @brief Guarda a informação no config
     */
    void GuardaConfig();

private slots:
    void on_btn_executar_clicked();

    void on_btn_salvar_clicked();

    void on_btn_folder_clicked();

    void on_btn_conectar_clicked();

    void on_cbox_servidor_editTextChanged();

    void on_txt_utilizador_textEdited();

    void on_txt_password_textEdited();

    void on_btn_sobre_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
