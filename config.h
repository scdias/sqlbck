#ifndef CONFIG_H
#define CONFIG_H
#include <QString>

/**
 * @brief Classe responsável por gerir a informação no ficheiro de configuração
 */
class config
{
private:
    /**
     * @brief Versão da estrutura da base de dados config
     */
    static QString Versao;

    /**
     * @brief Servidor SQL
     */
    static QString Servidor;

    /**
     * @brief Utilizador da autenticação SQL
     */
    static QString Utilizador;

    /**
     * @brief Password utilizado na password SQL
     */
    static QString Password;

    /**
     * @brief Indica se a informação da autenticação foi ja verificada
     */
    static bool Valido;

    /**
     * @brief Diretorio para onde serão feitas as cópias SQL
     */
    static QString Diretorio;

    /**
     * @brief Indica se utiliza a encriptação OpenPGP nos ficheiros de cópias
     */
    static bool Encriptacao;

    /**
     * @brief Indica se utiliza as funções de compactação do Servidor SQL
     */
    static bool Compactacao;

    /**
     * @brief Indica se a Edição do SQL Server é Express ou Profissional
     */
    static bool EdicaoProfissional;

    /**
     * @brief Indica se o sistema tem instalado OpenPGP
     */
    static bool OpenPGP;

    /**
     * @brief Define se utiliza a geração de TimeStamp no nome do ficheiro
     */
    static bool Timestamp;

    /**
     * @brief Define se o ficheiro de backup sera compactado em zip
     */
    static bool CompactacaoZip;

    /**
     * @brief Cria Estrutura da base de dados de configuração
     */
    static void CriarEstrutura();

    /**
     * @brief Função para upgrade da base de Dados Config para V1.0.1
     */
    static void UpgradeDbV1_0_1();

    /**
     * @brief Função para upgrade da base de Dados Config para V1.0.2
     */
    static void UpgradeDbV1_0_2();

public:
    /**
     * @brief Gere a ligação ao ficheiro de dados
     * @return Objeto com a ligação com a base de dados SQLite
     */
    static void IniciarConfig();

    /**
     * @brief Fecha a ligação com a base de dados de configuração
     */
    static void FechaConfig();

    /**
     * @brief Le as configurações do config para variáveis
     */
    static void LerConfig();

    /**
     * @brief Guarda os valores da classe no ficheiro config
     */
    static void GuardaConfig();

    /**
     * @brief Versão da Estrutura da Base de dados
     * @return QString com a versão da Estrutura da Base de dados
     */
    static QString getVersao();

    /**
     * @brief Versão da Estrutura da Base de dados
     * @param value - Versão da Estrutura da Base de dados
     */
    static void setVersao(const QString &value);

    /**
     * @brief Servidor SQL que será feito as cópias de segurança
     * @return QString com Servidor SQL que será feito as cópias de segurança
     */
    static QString getServidor();

    /**
     * @brief Servidor SQL que será feito as cópias de segurança
     * @param value - Servidor SQL que será feito as cópias de segurança
     */
    static void setServidor(const QString &value);

    /**
     * @brief Utilizador usado na autenticação SQL
     * @return QString com utilizador usado na autenticação SQL
     */
    static QString getUtilizador();

    /**
     * @brief Utilizador usado na autenticação SQL
     * @param value - Utilizador usado na autenticação SQL
     */
    static void setUtilizador(const QString &value);

    /**
     * @brief Password usado na autenticação SQL
     * @return QString com password usado na autenticação SQL
     */
    static QString getPassword();

    /**
     * @brief Password usado na autenticação SQL
     * @param value - Password usado na autenticação SQL
     */
    static void setPassword(const QString &value);

    /**
     * @brief Retorna se os dados de autenticação já foram verificados
     * @return bool - True se valido e false se incorretos
     */
    static bool getValido();

    /**
     * @brief Define se os dados de autenticação ja foram verificados
     * @param value - bool - True se valido e false se incorretos
     */
    static void setValido(bool value);

    /**
     * @brief Diretório para onde serão colocados os ficheiros de cópia
     * @return QString com diretório para onde serão colocados os ficheiros de cópia
     */
    static QString getDiretorio();

    /**
     * @brief Diretório para onde serão colocados os ficheiros de cópia
     * @param value - Diretório para onde serão colocados os ficheiros de cópia
     */
    static void setDiretorio(const QString &value);

    /**
     * @brief Define se utiliza a encriptação com OpenPGP
     * @return bool com o valor se utiliza a encriptação com OpenPGP
     */
    static bool getEncriptacao();

    /**
     * @brief Define se utiliza a encriptação com OpenPGP
     * @param value - utiliza a encriptação com OpenPGP
     */
    static void setEncriptacao(bool value);

    /**
     * @brief Define se utiliza função de compactação de cópias do servidor SQL
     * @return bool se utiliza função de compactação de cópias do servidor SQL
     */
    static bool getCompactacao();

    /**
     * @brief Define se utiliza função de compactação de cópias do servidor SQL
     * @param value - se utiliza função de compactação de cópias do servidor SQL
     */
    static void setCompactacao(bool value);

    /**
     * @brief Indica se a Edição do SQL Server é Express ou Profissional
     * @return bool - True -> Edição Profissional || False -> Edição Express
     */
    static bool getEdicaoProfissional();

    /**
     * @brief Indica se a Edição do SQL Server é Express ou Profissional
     * @param value - bool - True -> Edição Profissional || False -> Edição Express
     */
    static void setEdicaoProfissional(bool value);

    /**
     * @brief Indica se o sistema tem instalado OpenPGP
     * @return bool - True -> OpenPGP instalado || False -> OpenPGP não encontrado
     */
    static bool getOpenPGP();

    /**
     * @brief Indica se o sistema tem instalado OpenPGP
     * @param value - bool - True -> OpenPGP instalado || False -> OpenPGP não encontrado
     */
    static void setOpenPGP(bool value);

    /**
     * @brief Define se utiliza a geração de TimeStamp no nome do ficheiro
     * @return True se utiliza TimeStamp, False se não utiliza
     */
    static bool getTimestamp();

    /**
     * @brief Define se utiliza a geração de TimeStamp no nome do ficheiro
     * @param True se utiliza TimeStamp, False se não utiliza
     */
    static void setTimestamp(bool value);
    static bool getCompactacaoZip();
    static void setCompactacaoZip(bool value);
};

#endif // CONFIG_H
