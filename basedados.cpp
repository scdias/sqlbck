#include "basedados.h"

QString BaseDados::getNome()
{
    return nome;
}

void BaseDados::setNome(QString &value)
{
    nome = value;
}

int BaseDados::getTamanho_mb()
{
    return tamanho_mb;
}

void BaseDados::setTamanho_mb(int value)
{
    tamanho_mb = value;
}

BaseDados::BaseDados()
{

}

BaseDados::BaseDados(QString nome, int tamanho_mb)
{
    this->nome = nome;
    this->tamanho_mb = tamanho_mb;
}
